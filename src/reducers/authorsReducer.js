import { types } from '../types/types';

const initialState = {
    authors: [],
    active: null
}

export const authorsReducer = ( state = initialState, action ) => {

    switch ( action.type ) {
        case types.authorsLoad:
            return {
                ...state,
                authors: [...action.payload]
            }
        
        case types.authorsAddNew:
            return {
                ...state,
                authors: [ action.payload, ...state.authors ]
            }
        
        case types.authorsUpdate:
            return {
                ...state,
                authors: state.authors.map(
                    author => author.id === action.payload.id
                        ? action.payload.author
                        : author
                )
            }
           
        case types.authorsActive:
            return {
                ...state,
                active: {
                    ...action.payload
                }
            }
    
        case types.authorsDelete:
            return {
                ...state,
                active: null,
                authors: state.authors.filter( author => author.id !== action.payload )
            }
    
        default:
            return state;
    }

}