import { types } from '../types/types';

const initialState = {
    books: [],
    active: null
}

export const booksReducer = ( state = initialState, action ) => {

    switch (action.type) {
        case types.booksLoad:
            return {
                ...state,
                books: [...action.payload]
            }
        
        case types.booksActive:
            return {
                ...state,
                active: {
                    ...action.payload
                }
            }
        
        case types.booksAddNew:
            return {
                ...state,
                books: [ action.payload, ...state.books ]
            }
        
        case types.booksUpdate:
            return {
                ...state,
                books: state.books.map(
                    book => book.id === action.payload.id
                        ? action.payload.book
                        : book
                )
            }
        
        case types.booksDelete:
            return {
                ...state,
                active: null,
                books: state.books.filter( book => book.id !== action.payload )
            }
    
        default:
            return state;
    }

}