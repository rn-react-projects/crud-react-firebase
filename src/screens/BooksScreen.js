import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import BookForm from '../components/BookForm';

import { useForm } from '../hooks/useForm';

import { setActiveBook, startDeleting } from '../actions/books';

import { toast } from 'react-toastify';


//rn: Vista Libros
const BooksScreen = () => {

    const dispatch = useDispatch();

    const { books: booksState, ui: uiState } = useSelector(state => state);
    const { books } = booksState;
    const { loading } = uiState;

    const initialStateValues = {
        search: ""
    };

    const [ formValues, handleInputChange ] = useForm(initialStateValues);
    const { search } = formValues;

    //rn: Eliminar un libro de Firebase
    const onDeleteBook = async (id) => {
        if (window.confirm("Are you sure you want to delete this Book?")) {
          dispatch( startDeleting(id) );
          toast("Book removed successfully", {
            type: "error",
            autoClose: 2000
          });
        }
    };

    const handleActiveBook = ( id, book ) => {
        dispatch( setActiveBook(id, book) );
    }

    return (
        <div className="container">
            <div className="col-md-12">
                <h2 className="text-center">Book's Table</h2>
            </div>
            <div className="row">
                <div className="col-md-12 col-lg-4 p-2">
                    <BookForm />
                </div>
                <div className="col-md-12 col-lg-8 p-2">
                    <div className="form-group input-group">
                    <div className="input-group-text bg-light">
                        <i className="material-icons">search</i>
                    </div>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Search ..."
                        value={ search }
                        name="search"
                        onChange={ handleInputChange }
                    />
                </div>
                { 
                    loading 
                    ? <div className="col-md-12 col-lg-8 p-2">Loading...</div>
                    : books.filter((book) => {
                        if (!search) return true;
                        if (book.bookname.includes(search)) { return true; }
                        if (book.author.includes(search)) { return true; }
                        return false;
                    }).map((book) => (
                        <div className="card mb-1" key={book.id}>
                            <div className="card-body">
                                <div className="d-flex justify-content-between">
                                    <h4>{book.bookname}</h4>
                                    <div>
                                        <i className="material-icons text-danger" onClick={() => onDeleteBook(book.id)}>close</i>
                                        <i className="material-icons" onClick={ () => handleActiveBook( book.id, book ) }>create</i>
                                    </div>
                                </div>
                                <h6>Author: {book.author}</h6>
                                <h6>Editorial: {book.editorial}</h6>
                                <h6>Language: {book.language}</h6>
                                <p>{book.description}</p>
                            </div>
                        </div>
                    ))
                }
            </div>
            </div>
        </div>
    );
}

export default BooksScreen;