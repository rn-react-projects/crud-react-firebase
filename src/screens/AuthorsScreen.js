import React from "react";
import { useSelector, useDispatch } from 'react-redux';

import AuthorForm from '../components/AuthorForm';
import { Link } from 'react-router-dom';

import { toast } from "react-toastify";

import { setActiveAuthor, startDeleting } from "../actions/authors";

import '../scss/AuthorStyle.scss';

//rn: Vista Autores
const AuthorsScreen = () => {

    const dispatch = useDispatch();

    const { books: booksState , authors: authorsState, ui } = useSelector(state => state);
    const { books } = booksState;
    const { authors } = authorsState;
    const { loading } = ui;

    //rn: funcion para eliminar un autor
    const onDeleteAuthor = async (id, name) => {
        if (window.confirm("Are you sure you want to delete this Author?")) {
            //rn: revisamos que no tenga ningun libro registrado  
            var found = books.filter((book) => book.author.includes(name));
            //rn: sin include arroja length 0, podemos eliminarlo     
            if (found.length === 0) {
                dispatch( startDeleting(id) );
                toast("Author removed successfully", {
                  type: "error",
                  autoClose: 2000
                });
            } else {
                toast("We cannot delete this record as it is related to some books.", {
                    type: "error",
                    autoClose: 2000
                });
            }
        }
    };
    
    const handleActiveAuthor = (id, author) => {
        dispatch( setActiveAuthor( id, author ) );
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12 col-lg-4">
                    <Link to="/books" className="authorBackLink"><i className="material-icons">keyboard_arrow_left</i>Back to Books</Link>
                </div>
                <div className="col-md-12 col-lg-8">
                    <h2 className="text-center">Author's Table</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12 col-lg-4 p-2">
                    <AuthorForm />
                </div>
                <div className="col-md-12 col-lg-8 p-2">
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Nationality</th>
                                <th scope="col">Birth Date</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        {   
                            loading 
                                ? <tbody><tr><td colSpan="4" align="center">Loading...</td></tr></tbody>
                                : authors.map(( author ) => (
                                    <tbody key={author.id}>
                                        <tr>
                                            <th scope="row">{author.name}</th>
                                            <td>{author.nationality}</td>
                                            <td>{author.birthDate}</td>
                                            <td>
                                                <i className="material-icons text-danger" onClick={() => onDeleteAuthor(author.id, author.name)}>close</i>
                                                <i className="material-icons" onClick={ () => handleActiveAuthor( author.id, author) }>create</i>
                                            </td>
                                        </tr>
                                    </tbody>
                                ))
                        }
                    </table>
                </div>
            </div>
        </div>
    );
}

export default AuthorsScreen;