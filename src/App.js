import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';

import { useDispatch } from 'react-redux';

import BooksScreen from './screens/BooksScreen';
import AuthorsScreen from './screens/AuthorsScreen';

import { startLoadingBooks } from './actions/books';
import { startLoadingAuthors } from './actions/authors';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import 'bootstrap';

import './scss/NavBar.scss';
import './scss/NavBarMobile.scss';

function App() {

    const dispatch = useDispatch();

    const [collapse, setIsToggleOn] = useState(false);

    useEffect(() => {

        dispatch( startLoadingBooks() );
        dispatch( startLoadingAuthors() );

    }, [dispatch]);

    return (
        // <BrowserRouter basename='/crud-react-firebase/'>
        <BrowserRouter>
        <header className="main_menu p-4" id="header">
                <nav className="navbar navbar-expand-lg container">
                    <button className="navbar-toggler" type="button" data-toggle="dropdown" data-target="#navbar-mobile"
                        aria-expanded="true" aria-controls="navbarResponsive">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse col-md-12" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link className="nav-link" to="/books">Books</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/authors">Authors</Link>
                            </li>
                        </ul>
                    </div>
                    <div className={`navbar-mobile collapse ${collapse ? '' : 'hide'}`} id="navbar-mobile">
                        <div className="container">
                            <div className="col-12 text-right">
                                <button className="close mt-2" type="button" onClick={ e => setIsToggleOn(!collapse) }>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <div className="mm-logo-container">
                                    <h2>React CRUD Firebase</h2>
                                </div>
                            </div>
                            <div className="col-12 menu-mobile mt-3">
                                <ul className="mm-navbar-list">
                                    <li className="mm-nav-item" onClick={ e => setIsToggleOn(!collapse) }>
                                        <Link className="mm-nav-link" to="/books">Books<span className="sr-only">(current)</span></Link>
                                    </li>
                                    <li className="mm-nav-item" onClick={ e => setIsToggleOn(!collapse) }>
                                        <Link className="mm-nav-link" to="/authors">Authors</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
            <div className="container p-4">
                <Route path="/" exact={true} component={BooksScreen} />
                <Route path="/books" component={BooksScreen} />
                <Route path="/authors" component={AuthorsScreen} />
                <ToastContainer />          
            </div>
        </BrowserRouter>
    );
}
    
export default App;
