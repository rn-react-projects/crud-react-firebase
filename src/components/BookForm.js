import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from 'react-router-dom';

import { useForm } from '../hooks/useForm';

import 'bootstrap';
import 'material-icons';
import { addOrEditBook, setActiveBook } from "../actions/books";

//rn: Componente Formulario de Libros y Lista
const BookForm = () => {

    const dispatch = useDispatch();

    const { books: booksState , authors: authorsState } = useSelector(state => state);
    let { active } = booksState;
    const { authors } = authorsState;
    
    const initialStateValues = {
        id: 'New',
        bookname: '',
        author: '',
        editorial: '',
        language: '',
        description: ''
    };

    if ( !active ) {
        active = initialStateValues;
    } 

    const [ formValues, handleInputChange, reset ] = useForm(active);
    const { id, bookname, author, editorial, language, description } = formValues;

    const currentId = useRef( id );

    useEffect(() => {

        if ( active.id !== currentId.current ) {
            reset( active );
            currentId.current = active.id;
        }

    }, [active, reset]);

    useEffect(() => {
        dispatch( setActiveBook( formValues.id, {...formValues} ) );
    }, [formValues, dispatch])
    
    //rn: Enviamos formulario y llamamos a la funcion addOrEditBook
    const handleSubmit = (e) => {
        e.preventDefault();
        
        dispatch( addOrEditBook( formValues ) );
        reset(initialStateValues);
    };

    return (
        <form className="card card-body" onSubmit={ handleSubmit }>
            <div className="form-group input-group">
                <div className="input-group-text bg-light">
                    <i className="material-icons">book</i>
                </div>
                <input
                    type="text"
                    className="form-control"
                    placeholder="Book Name"
                    name="bookname"
                    value={ bookname }
                    onChange={ handleInputChange }
                    autoComplete="off"
                />
            </div>
            <div className="form-group">
                <div className="input-group">
                    <div className="input-group-prepend">
                        <label className="input-group-text bg-light" htmlFor="author"><i className="material-icons">person</i></label>
                    </div>
                    <select 
                        className="custom-select" 
                        id="author" 
                        name="author" 
                        value={ author } 
                        onChange={ handleInputChange } 
                    >
                        <option value="">Select an Author</option>
                        {authors.map((author) => (
                            <option value={author.name} key={author.id}>{author.name}</option>
                        ))}
                    </select>
                    <div className="input-group-append"
                        data-toggle="tooltip" 
                        data-placement="bottom" 
                        title="Add a new author">
                        <Link to="/authors" className="btn btn-outline-secondary">+</Link>
                    </div>
                </div>
            </div>
            <div className="form-group input-group">
                <div className="input-group-text bg-light">
                    <i className="material-icons">domain</i>
                </div>
                <input
                    type="text"
                    className="form-control"
                    placeholder="Editorial"
                    value={ editorial }
                    name="editorial"
                    onChange={ handleInputChange }
                />
            </div>
            <div className="form-group input-group">
                <div className="input-group-text bg-light">
                    <i className="material-icons">language</i>
                </div>
                <input
                    type="text"
                    className="form-control"
                    placeholder="Language"
                    value={ language }
                    name="language"
                    onChange={ handleInputChange }
                />
            </div>
            <div className="form-group input-group">
                <textarea 
                    className="form-control" 
                    id="description" 
                    rows="3"
                    value={ description }
                    placeholder="Description"
                    name="description"
                    onChange={ handleInputChange }
                    ></textarea>
            </div>

            <button className="btn btn-primary btn-block">
                { active.id === 'New' ? "Save" : "Update"}
            </button>
        </form>
    );
}

export default BookForm;