import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

import { useForm } from '../hooks/useForm';

import 'material-icons';
import { addOrEditAuthor, setActiveAuthor } from "../actions/authors";

//rn: Componente Formulario de Autores y Lista
const AuthorForm = () => {

    const dispatch = useDispatch();

    let { active } = useSelector(state => state.authors);
    
    const initialStateValuesAuthors = {
        id: 'New',
        name: '',
        nationality: '',
        birthDate: ''
    };

    if ( !active ) {
        active = initialStateValuesAuthors;
    }

    const [ formValues, handleInputChange, reset ] = useForm( active );
    const { id, name, nationality, birthDate } = formValues;

    const currentAuthorId = useRef( id );

    useEffect(() => {

        if ( active.id !== currentAuthorId.current ) {
            reset( active );
            currentAuthorId.current = active.id;
        }

    }, [active, reset]);

    useEffect(() => {
        dispatch( setActiveAuthor( formValues.id, {...formValues} ) );
    }, [formValues, dispatch]);

    //rn: Enviamos formulario y llamamos a la funcion addOrEditAuthor en AuthorScreen
    const handleSubmit = (e) => {
        e.preventDefault();
        
        dispatch( addOrEditAuthor( formValues ) );
        reset( initialStateValuesAuthors );
    };

    return (
        <>
            <form className="card card-body" onSubmit={ handleSubmit }>
                <div className="form-group input-group">
                    <div className="input-group-text bg-light">
                        <i className="material-icons">person</i>
                    </div>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Author Name"
                        name="name"
                        value={ name }
                        onChange={handleInputChange}
                    />
                </div>
                <div className="form-group input-group">
                    <div className="input-group-text bg-light">
                        <i className="material-icons">language</i>
                    </div>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Nationality"
                        name="nationality"
                        value={ nationality }
                        onChange={ handleInputChange }
                    />
                </div>
                <div className="form-group input-group">
                    <div className="input-group-text bg-light">
                        <i className="material-icons">calendar_today</i>
                    </div>
                    <input
                        type="date"
                        className="form-control"
                        placeholder="Birth Date"
                        name="birthDate"
                        value={ birthDate }
                        onChange={ handleInputChange }
                    />
                </div>
                <button className="btn btn-primary btn-block">
                    { active.id === 'New' ? 'Save' : 'Update' }
                </button>
            </form>
        </>
    );
}

export default AuthorForm;