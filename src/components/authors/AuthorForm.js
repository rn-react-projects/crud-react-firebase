import React, { useEffect } from "react";
import { useForm } from "../../hooks/useForm";

import 'material-icons';

//rn: Componente Formulario de Autores y Lista
const AuthorForm = ({ currentId, addOrEditAuthor }) => {
    
    const initialStateValuesAuthors = {
        name: "",
        nationality: "",
        birthDate: ""
    };

    const [ formValues, handleInputChange, reset ] = useForm( initialStateValuesAuthors );

    const { name, nationality, birthDate } = formValues;

    //rn: Enviamos formulario y llamamos a la funcion addOrEditAuthor en AuthorScreen
    const handleSubmit = (e) => {
        e.preventDefault();
        
        addOrEditAuthor(formValues);
        reset();
    };

    //rn: Obtenermos un autor por ID
    const getAuthorById = async (id) => {
        // const doc = await db.collection("authors").doc(id).get();

        // setValues({ ...doc.data() });
    };

    useEffect(() => {
        //rn: Comprobamos si ID es igual a empty para modificar el estado Save o Update
        if (currentId === "") {
          
        } else {
          getAuthorById(currentId);
        }
      }, [currentId]);

    return (
        <>
            <form className="card card-body" onSubmit={handleSubmit}>
                <div className="form-group input-group">
                    <div className="input-group-text bg-light">
                        <i className="material-icons">person</i>
                    </div>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Author Name"
                        value={name}
                        name="name"
                        onChange={handleInputChange}
                    />
                </div>
                <div className="form-group input-group">
                    <div className="input-group-text bg-light">
                        <i className="material-icons">language</i>
                    </div>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Nationality"
                        value={nationality}
                        name="nationality"
                        onChange={handleInputChange}
                    />
                </div>
                <div className="form-group input-group">
                    <div className="input-group-text bg-light">
                        <i className="material-icons">calendar_today</i>
                    </div>
                    <input
                        type="date"
                        className="form-control"
                        placeholder="Birth Date"
                        value={birthDate}
                        name="birthDate"
                        onChange={handleInputChange}
                    />
                </div>
                <button className="btn btn-primary btn-block">
                    {currentId === "" ? "Save" : "Update"}
                </button>
            </form>
        </>
    );
}

export default AuthorForm;