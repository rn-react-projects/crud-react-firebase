import { types } from "../types/types";
import { db } from "../firebase/firebaseConfig";
import { loadAuthors } from "../helpers/loadAuthors";
import { finishLoading, startLoading } from "./ui";

export const startLoadingAuthors = () => {
    return async ( dispatch ) => {
        dispatch( startLoading() );
        const authors = await loadAuthors();
        dispatch( setAuthors( authors ) );
        dispatch( finishLoading() );

    }
}

export const setAuthors = ( authors ) => ({
    type: types.authorsLoad,
    payload: authors
});

export const addOrEditAuthor = ( author ) => {
    return async ( dispatch ) => {
        dispatch( startLoading() );
        if ( author.id === 'New' ) {
            delete author.id;
            const doc = await db.collection('authors').add( author );
            dispatch( addNewAuthor( doc.id, author ) );
        } else {
            await db.collection('authors').doc(author.id).update( author );
            dispatch( updateAuthor( author.id, author) );
        }
        dispatch( finishLoading() );
    }
}

export const addNewAuthor = ( id, author ) => ({
    type: types.authorsAddNew,
    payload: {
        id,
        ...author
    }
});

export const updateAuthor = ( id, author ) => ({
    type: types.authorsUpdate,
    payload: {
        id,
        author: {
            id,
            ...author
        }
    }
});

export const setActiveAuthor = ( id, author ) => ({
    type: types.authorsActive,
    payload: {
        id,
        ...author
    }
});

export const startDeleting = ( id ) => {
    return async ( dispatch ) => {
        await db.collection('authors').doc(id).delete();
        dispatch( deleteAuthor(id) );
    }
}

export const deleteAuthor = ( id ) => ({
    type: types.authorsDelete,
    payload: id
});