import { db } from "../firebase/firebaseConfig";
import { types } from "../types/types";
import { loadBooks } from "../helpers/loadBooks";
import { finishLoading, startLoading } from "./ui";

// export const addOrEditBook = async (booksObject, currentId) => {
//     return (dispatch) => {
//         if (currentId === "") {
//             await db.collection("books").doc().set(booksObject);
//             toast("New Book Added", {
//             type: "success",
//         });
//         } else {
//             await db.collection("books").doc(currentId).update(booksObject);
//             toast("Book Updated Successfully", {
//             type: "info",
//         });
//             setCurrentId("");
//         }        
//     }
// }

export const addOrEditBook = ( book ) => {
    return async ( dispatch ) => {
        dispatch( startLoading() );
        if ( book.id === 'New' ) {
            delete book.id;
            const doc = await db.collection('books').add( book );
            dispatch( addNewBook( doc.id, book) );
        } else {
            await db.collection('books').doc(book.id).update( book );
            dispatch ( updateBook( book.id, book ) );
        }
        dispatch( finishLoading() );
    }
}

export const addNewBook = ( id, book ) => ({
    type: types.booksAddNew,
    payload: {
        id,
        ...book
    }
});

export const updateBook = ( id, book ) => ({
    type: types.booksUpdate,
    payload: {
        id,
        book: {
            id,
            ...book
        }
    }
});

export const startLoadingBooks = () => {

    return async ( dispatch ) => {

        dispatch( startLoading() );

        const books = await loadBooks();
        dispatch( setBooks( books ) );
        dispatch( finishLoading() );

    }
}

export const setBooks = ( books ) => ({
    type: types.booksLoad,
    payload: books
});

export const setActiveBook = ( id, book ) => ({
    type: types.booksActive,
    payload: {
        id,
        ...book
    }
});

export const startDeleting = ( id ) => {
    return async( dispatch ) => {
        await db.collection('books').doc(id).delete();
        dispatch( deleteNote(id) );
    }
}

export const deleteNote = (id) => ({
    type: types.booksDelete,
    payload: id
});