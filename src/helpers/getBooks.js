import { db } from "../firebase";

// const getBooks = async () => {
//     db.collection("books").onSnapshot((querySnapshot) => {
//       const docs = [];
//       querySnapshot.forEach((doc) => {
//         docs.push({ ...doc.data(), id: doc.id });
//       });
//       setBooks(docs);
//     });
// };

export const getBooks = async () => {
    const resp = await db.collection("books");
    const { docs } = await resp.get();

    const books = docs.map( book => {
        return {
            id: book.id,
            author: book.data().author,
            bookname: book.data().bookname,
            description: book.data().description,
            editorial: book.data().editorial,
            language: book.data().language,
        };
    });

    return books;
}