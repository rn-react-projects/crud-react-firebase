import { db } from "../firebase/firebaseConfig";

export const loadBooks = async () => {
    
    const booksSnap = await db.collection('books').get();
    const books = [];

    booksSnap.forEach( snapChild => {
        books.push({
            id: snapChild.id,
            ...snapChild.data()
        });
    });
    
    return books;

}