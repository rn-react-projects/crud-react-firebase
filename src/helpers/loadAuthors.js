import { db } from "../firebase/firebaseConfig";

export const loadAuthors = async () => {
    
    const authorsSnap = await db.collection('authors').get();
    const authors = [];

    authorsSnap.forEach( snapChild => {
        authors.push({
            id: snapChild.id,
            ...snapChild.data()
        });
    });
    
    return authors;

}