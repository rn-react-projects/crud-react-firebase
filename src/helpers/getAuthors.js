import { db } from '../firebase/firebaseConfig';

export const getAuthors = async() => {
    const resp = await db.collection("authors");
    const { docs } = await resp.get();

    const authors = docs.map( author => {
        return {
            id: author.id,
            name: author.data().name,
            birthDate: author.data().birthDate,
            nationality: author.data().nationality,
        };
    });

    return authors;
}