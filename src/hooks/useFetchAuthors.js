import { useEffect, useState } from 'react';
import { getAuthors } from '../helpers/getAuthors';

export const useFetchAuthors = () => {
    const [state, setState] = useState({
        data: [],
        loading: true,
    });

    useEffect(() => {
        getAuthors()
            .then( auths => {
                setState({
                    data: auths,
                    loading: false,
                });
            });
    }, []);
    
    return state;
}