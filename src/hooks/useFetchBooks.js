import { useEffect, useState } from 'react';
import { getBooks } from '../helpers/getBooks';

export const useFetchBooks = () => {
    const [state, setState] = useState({
        data: [],
        loading: true,
    });

    useEffect(() => {
        setState({ data: [], loading: true });

        getBooks()
            .then(books => {
                setState({
                    data: books,
                    loading: false,
                });
            })
            .catch( () => {
                setState({
                    data: [],
                    loading: false,
                });                
            });
    }, []);

    return state;
}
