import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBaEBjcxgroteS0JnMe0U4Sk5GaDyGMsKg",
    authDomain: "crud-react-91da7.firebaseapp.com",
    projectId: "crud-react-91da7",
    storageBucket: "crud-react-91da7.appspot.com",
    messagingSenderId: "735557999485",
    appId: "1:735557999485:web:6c44cc97b4a74447eb8a14"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export {
    db,
    firebase
}