export const types = {

    uiStartLoading: '[UI] Start Loading',
    uiFinishLoading: '[UI] Finish Loading',

    booksAddNew: '[BOOKS] New Book',
    booksDelete: '[BOOKS] Delete Book',
    booksActive: '[BOOKS] Set active Book',
    booksUpdate: '[BOOKS] Update Book',
    booksLoad: '[BOOKS] Load Books',

    authorsAddNew: '[AUTHORS] New Author',
    authorsDelete: '[AUTHORS] Delete Author',
    authorsActive: '[AUTHORS] Set active Author',
    authorsUpdate: '[AUTHORS] Update Author',
    authorsLoad: '[AUTHORS] Load Authors'

}